#!/bin/bash
# All combinations of gpu_configs, cuda executables and arguments.

declare -a gpu_configs=(
    SM7_QV100
    SM6_TITANX
    SM7_QV100_8K_8_8
    SM7_QV100_16K_16_8
    SM7_QV100_32K_32_8
    SM7_QV100_64K_64_8
    SM7_QV100_64K_64_8_8SM
    SM7_QV100_64K_64_8_16SM
    SM7_QV100_64K_64_8_32SM
)

# Combination of an executable name, its path and its arguments.
declare -a name_exe_and_args=(
    kmeans,"\$HOME/Documents/workspace/gpu-rodinia/cuda/kmeans/kmeans","-i \$HOME/Documents/workspace/gpu-rodinia/data/kmeans/800_34.txt"
    srad_v1,"\$HOME/Documents/workspace/gpu-rodinia/cuda/srad/srad_v1/srad","2 0.5 128 128 \$HOME/Documents/workspace/gpu-rodinia/data/srad/image.pgm"
    srad_v2,"\$HOME/Documents/workspace/gpu-rodinia/cuda/srad/srad_v2/srad","256 256 0 127 0 127 0.5 2 \$HOME/Documents/workspace/gpu-rodinia/data/srad/results_v2_256_256_0_127_0_127_0.5_2.txt"
    hotspot_256,"\$HOME/Documents/workspace/gpu-rodinia/cuda/hotspot/hotspot","256 2 1 \$HOME/Documents/workspace/gpu-rodinia/data/hotspot/temp_256 \$HOME/Documents/workspace/gpu-rodinia/data/hotspot/power_256 \$HOME/Documents/workspace/gpu-rodinia/data/hotspot/results_256.txt"
    hotspot_512,"\$HOME/Documents/workspace/gpu-rodinia/cuda/hotspot/hotspot","512 2 2 \$HOME/Documents/workspace/gpu-rodinia/data/hotspot/temp_512 \$HOME/Documents/workspace/gpu-rodinia/data/hotspot/power_512 \$HOME/Documents/workspace/gpu-rodinia/data/hotspot/results_512.txt"
    lud,"\$HOME/Documents/workspace/gpu-rodinia/cuda/lud/cuda/lud_cuda","-s 128 -v"
    bfs,"\$HOME/Documents/workspace/gpu-rodinia/cuda/bfs/bfs","\$HOME/Documents/workspace/gpu-rodinia/data/bfs/graph32k.txt \$HOME/Documents/workspace/gpu-rodinia/data/bfs/results_32k.txt"
    pathfinder,"\$HOME/Documents/workspace/gpu-rodinia/cuda/pathfinder/pathfinder","10000 100 20"
    nw,"\$HOME/Documents/workspace/gpu-rodinia/cuda/nw/needle","288 10"
    gaussian,"\$HOME/Documents/workspace/gpu-rodinia/cuda/gaussian/gaussian","-s 80 -f \$HOME/Documents/workspace/gpu-rodinia/data/gaussian/matrix80.txt -q"
    backprop,"\$HOME/Documents/workspace/gpu-rodinia/cuda/backprop/backprop","8192"
)
