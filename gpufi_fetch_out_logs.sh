#!/bin/bash
# Script to get all reference logs for each analyzed benchmark from
# a remote host. I used it when I wanted to analyze the logs to get
# statistics per benchmark (e.g. Hits, miss etc)
set -ex

source ./benchmark_config.sh
REMOTE_HOST="Prixtis"
LOCAL_BASE_DIR="$HOME/Sync/Documents/CTN_ENG/Thesis/gpuFI-4/logs"
# Single quote so that it doesn't expand variables locally, we expect to find "gpufi_utils.sh" in there.
GPUFI_BASE_PATH='$HOME/workspace/gpuFI-4'
# Function to sanitize args to a folder name
# From here: https://stackoverflow.com/a/44811468/6562491
# echoes "null" if no input given.
_sanitize_string() {
    local s="${*:-null}"     # receive input in first argument
    s="${s//[^[:alnum:]]/-}" # replace all non-alnum characters to -
    s="${s//+(-)/-}"         # convert multiple - to single -
    s="${s/#-/}"             # remove - from start
    s="${s/%-/}"             # remove - from end
    echo "${s,,}"            # convert to lowercase
}

for exe_args in "${name_exe_and_args[@]}"; do
    for gpu_config in "${gpu_configs[@]}"; do
        name=$(echo $exe_args | cut -d',' -f1)
        exe=$(echo $exe_args | cut -d',' -f2)
        args=$(echo $exe_args | cut -d',' -f3)

        remote_benchmark_dir=$(dirname $exe)
        # To get the proper sanitized path from the args, we need to run the function
        # on the remote host, so that HOME can be expanded properly.
        # MACHETE
        echo -n "Getting remote results path for benchmark $name from $REMOTE_HOST..."
        remote_cmd=$(echo "cd "${GPUFI_BASE_PATH}" && source gpufi_utils.sh && _sanitize_string \""$args'"')
        sanitized_args=$(ssh "$REMOTE_HOST" $remote_cmd)

        # The final path where out.log is in the remote host.
        # We need to evaluate the $HOME var remotely first.
        remote_log_filepath=$(ssh $REMOTE_HOST 'echo '${remote_benchmark_dir}"/.gpufi/$gpu_config/$sanitized_args/out.log")
        echo "Done."

        # Don't use sanitized args here, so that we can use the same dir for all hosts
        results_dir="$LOCAL_BASE_DIR/$name/$gpu_config/$(_sanitize_string $args)"
        mkdir -p "$results_dir"
        echo -n "Copying remote file $REMOTE_HOST:${remote_log_filepath}..."
        scp "$REMOTE_HOST":"$remote_log_filepath" "$results_dir/out.log" || echo "ERROR: Could not find remote file $remote_log_filepath"
        echo "Done."
    done
done
