#!/bin/bash

# Script for producing tex for all the available plots for sets hotness.
# If a specific PDF is not found, it's skipped.
# PDFs are produced from the jupyter notebook.
#
# Example: bash gpufi_sets_hotness_figures.sh  > sets_hotness_figures.tex
#
source ./benchmark_config.sh

for exe_args in "${name_exe_and_args[@]}"; do
    name=$(echo $exe_args | cut -d',' -f1)
    echo "\begin{figure}[ht]
    \caption{Aggregate L1I cache sets hotness per SM and per set for the $(echo $name | tr [a-z] [A-Z] | sed 's/_/\\_/') benchmark, for each GPU configuration}
    \label{fig:sets-hotness-${name}}"
    for gpu_config in "${gpu_configs[@]}"; do
        fig_filename="sets_hotness_${name}_${gpu_config}.pdf"
        if [ ! -f $fig_filename ]; then
            continue
        fi
        echo "  \begin{subfigure}[b]{0.5\textwidth}
        \centering
        \caption{$(echo $gpu_config | cut -d'_' -f2)}
        \includegraphics[page=1,width=\textwidth]{$fig_filename}
    \end{subfigure}"
    done
    echo "\end{figure}"
done
