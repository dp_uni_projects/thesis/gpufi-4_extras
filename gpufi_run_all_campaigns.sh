#!/bin/bash

# Yet another script for full campaigns for all configs and args
# Cannot use -e because We want to check a nonzero return code :/
set -x
source ./benchmark_config.sh

prev_dir=$PWD
GPUFI_SOURCE="$HOME/workspace/gpuFI-4"
NUM_RUNS=1000

analyze_executables() {
    # Run analysis in parallel?
    for gpu_config in "${gpu_configs[@]}"; do
        # gpu_config_file="$GPUFI_SOURCE/configs/tested-cfgs/$gpu_config/gpgpusim.config"
        for exe_args in "${name_exe_and_args[@]}"; do
            name=$(echo $exe_args | cut -d',' -f1)
            exe=$(echo $exe_args | cut -d',' -f2)
            args=$(echo $exe_args | cut -d',' -f3)
            echo "Starting campaign for $name, and GPU $gpu_config"
            bash gpufi_analyze_executable.sh CUDA_EXECUTABLE_PATH="$exe" CUDA_EXECUTABLE_ARGS="$args" GPU_ID=$gpu_config &
            pid=$!
            sleep 0.5
            # If analysis is not running and its exit code was 128
            # (i.e. analysis already done), continue
            if ! ps -p $pid; then
                wait $pid
                ret=$?
                if [ "$ret" -eq 128 ]; then
                    continue
                fi
            fi
            sleep 5
        done
    done
    wait
}

run_campaigns() {
    # Start campaigns
    for gpu_config in "${gpu_configs[@]}"; do
        # gpu_config_file="$GPUFI_SOURCE/configs/tested-cfgs/$gpu_config/gpgpusim.config"
        for exe_args in "${name_exe_and_args[@]}"; do
            name=$(echo $exe_args | cut -d',' -f1)
            exe=$(echo $exe_args | cut -d',' -f2)
            args=$(echo $exe_args | cut -d',' -f3)
            echo "Starting campaign for $name, and GPU $gpu_config"
            bash gpufi_campaign.sh CUDA_EXECUTABLE_PATH="$exe" CUDA_EXECUTABLE_ARGS="$args" GPU_ID="$gpu_config" NUM_RUNS=$NUM_RUNS KERNEL_INDICES=0 _NUM_AVAILABLE_CORES=$(nproc) DELETE_LOGS=1
        done
    done
}

### Main script
declare -a steps=(
    analyze_executables
    run_campaigns
)

# Create dynamic flags to selectively disable/enable steps of the analysis if needed.
# Those flags are named "do_" with the name of the function.
# We set those flags to 1 by default.
for step in "${steps[@]}"; do
    eval "do_${step}=1"
done

# Override variables from script
for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"
    eval "$KEY=\"$VALUE\""
done

cd "$GPUFI_SOURCE"
source "./startup.sh" release

# The actual procedure.
# For each step, check if the appropriate flag is enabled.
for step in "${steps[@]}"; do
    step_flag_name=do_$step
    if [ "${!step_flag_name}" -ne 0 ]; then
        echo "Step: $step"
        # Run the actual function
        eval "$step"
    else
        echo "Skipping step: $step"
    fi
done

cd $prev_dir
