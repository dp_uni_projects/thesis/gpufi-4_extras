#!/bin/bash

# gpuFI helper script to filter remote results and copy locally
# configs and logs for debugging.
# It is meant to be run once after the remote hosts have finished
# running a campaign, preferably with DELETE_LOGS=0.
# For each host in HOSTS It will parse all CSV files created by gpuFI
# in REMOTE_BASE_RESULTS_DIRS (that match the FILENAME_FILTER)
# and look for interesting run_ids. Then, it will use those run_ids to
# find the config it ran with and the log it produced in the REMOTE_BASE_GPUFI_DIRS.
#
# The filtering is done by copying remotly the gpufi_filter_interesting_results.sh
# script found in the same directory as the current script.
#
# It's preferable that you have already run ssh-copy-id to the remote host
# so that you won't have to type passwords all the time.
#
# Example:
# bash gpufi_harvest_remote_interesting_results_and_configs.sh HOSTS=home5593 REMOTE_BASE_RESULTS_DIRS=/home/mpliax/workspace/gpu-rodinia/cuda REMOTE_BASE_GPUFI_DIRS=/home/mpliax/workspace/gpuFI-4
#

set -e

# Semicolon-separated hostnames to connect to.
# e.g. user1@host1:user2@host2
HOSTS=

# Semicolon-separated paths to where to look for results. One for each hostname
# e.g. /path/to/host1/results:/path/to/host2/results
REMOTE_BASE_RESULTS_DIRS=

# gpuFI remote path, where configs and logs are to be found
REMOTE_BASE_GPUFI_DIRS=

# Filter for results csv files.
FILENAME_FILTER="*.csv"

# Local base path to store results
LOCAL_BASE_DIR=

# Script to copy to remote host and run for each CSV found
_FILTER_SCRIPT_PATH="./gpufi_filter_interesting_results.sh"

### Internal vars
_num_hosts=0
_num_remote_results_dirs=0
_num_remote_gpufi_dirs=0
# Path where the copied filter script will placed. Some undescores to signify
# how secret this file is.
_remote_tmp_filter_filename="/tmp/____$(basename $_FILTER_SCRIPT_PATH)"
_remote_tmp_aggregate_dir="/tmp/____gpufi_interesting_results"

preliminary_checks() {

    # Check hosts
    if [ -z "$HOSTS" ]; then
        echo "Please provide HOSTS to connect to."
        exit 1
    fi
    IFS=":" read -ra HOSTS <<<"$HOSTS"
    _num_hosts=$((${#HOSTS[*]}))

    # Check remote dirs
    if [ -z "$REMOTE_BASE_RESULTS_DIRS" ]; then
        echo "Please provide REMOTE_BASE_RESULTS_DIRS (one per host in HOSTS)."
        exit 1
    fi

    IFS=":" read -ra REMOTE_BASE_RESULTS_DIRS <<<"$REMOTE_BASE_RESULTS_DIRS"
    _num_remote_results_dirs=$((${#REMOTE_BASE_RESULTS_DIRS[*]}))

    if [ $_num_remote_results_dirs -ne $_num_hosts ]; then
        echo "Incompatible number of HOSTS ($_num_hosts) and REMOTE_BASE_RESULTS_DIRS ($_num_remote_results_dirs) supplied! Their numbers must match."
        exit 1
    fi

    # Check remote gpuFI dirs
    if [ -z "$REMOTE_BASE_GPUFI_DIRS" ]; then
        echo "Please provide REMOTE_BASE_GPUFI_DIRS (one per host in HOSTS)."
        exit 1
    fi
    IFS=":" read -ra REMOTE_BASE_GPUFI_DIRS <<<"$REMOTE_BASE_GPUFI_DIRS"
    _num_remote_gpufi_dirs=$((${#REMOTE_BASE_GPUFI_DIRS[*]}))

    if [ $_num_remote_gpufi_dirs -ne $_num_hosts ]; then
        echo "Incompatible number of HOSTS ($_num_hosts) and REMOTE_BASE_GPUFI_DIRS ($_num_remote_gpufi_dirs) supplied! Their numbers must match."
        exit 1
    fi

    if [ ! -f "$_FILTER_SCRIPT_PATH" ]; then
        echo "Could not find _FILTER_SCRIPT_PATH! No such file: $_FILTER_SCRIPT_PATH"
        exit 1
    fi

    if [ -z "$LOCAL_BASE_DIR" ]; then
        LOCAL_BASE_DIR=/tmp/gpufi_interesting_runs
        echo "No LOCAL_BASE_DIR specified, we will store results in $LOCAL_BASE_DIR."
    fi

    if [ ! -d "$LOCAL_BASE_DIR" ]; then
        mkdir -p "$LOCAL_BASE_DIR"
    fi

    for host in "${HOSTS[@]}"; do
        mkdir -p "$LOCAL_BASE_DIR/$host"
    done

}

# Copy filter script to remote hosts
remote_copy_filter_script() {
    for host in "${HOSTS[@]}"; do
        echo -n "Copying filter script ($(basename $_FILTER_SCRIPT_PATH)) to host $host..."
        scp $_FILTER_SCRIPT_PATH "$host":"$_remote_tmp_filter_filename" >/dev/null 2>&1
        echo "Done"
    done
}

# Cleanup filter script from remote hosts
remote_remove_filter_script() {
    for host in "${HOSTS[@]}"; do
        echo -n "Removing filter script ($(basename $_FILTER_SCRIPT_PATH)) from host $host..."
        ssh "$host" "rm -f \"$_remote_tmp_filter_filename\""
        echo "Done"
    done
}

aggregate_and_fetch_interesting_results() {
    host_index=0
    for host in "${HOSTS[@]}"; do
        remote_base_results_dir=${REMOTE_BASE_RESULTS_DIRS[$host_index]}
        remote_base_gpufi_dir=${REMOTE_BASE_GPUFI_DIRS[$host_index]}
        echo -n "Looking for $FILENAME_FILTER in $remote_base_results_dir @ $host..."
        files=$(ssh $host "
        find $remote_base_results_dir -name '$FILENAME_FILTER'
        ")
        OIFS=$IFS
        IFS=$'\n'
        files=($files)
        # files=("${files[0]}")
        IFS=$OIFS
        echo "Done, found ${#files[@]} files"

        # Run filter script onto all the csv files found, extract run_ids
        echo "Looking for interesting runs in $host."
        all_run_ids=$() # Array holding run_ids
        for file in "${files[@]}"; do
            echo "Searching in file $file"
            run_ids=$(ssh $host "bash $_remote_tmp_filter_filename RESULTS_CSV_FILEPATH=$file")
            if [ -n "$run_ids" ]; then
                all_run_ids+=($run_ids)
            fi
        done
        echo "Done, found ${#all_run_ids[@]} runs."
        _time_now=$(printf '%(%Y%m%d-%H%M%S)T' -1)
        _tar_filename="/tmp/gpufi_interesting_results_${host}_${_time_now}.tar.gz"

        # Using all the run_ids, grep inside all files in the base gpuFI-4 directory,
        # find .log and .config files. Copy them into a temp directory and
        # tar them.
        echo "Looking for logs & config files in $host and aggregating them..."
        ssh "$host" "mkdir -p $_remote_tmp_aggregate_dir
        rm -f /tmp/log.out
        run_ids=(${all_run_ids[@]})
        for run_id in \"\${run_ids[@]}\"; do
            echo \"Looking for files related to run \$run_id\"
            files=\$(grep -or \$run_id $remote_base_gpufi_dir | uniq)
            if [ \"\$files\" = \"\" ]; then
                echo \"No files for run \$run_id!\"
                files=\$(find $remote_base_results_dir -name \"\$run_id*\" -type f | grep -v results.csv | uniq)
            fi
            for file in \$files; do
                echo \"Found file \$file\"
                file=\$(echo \$file | cut -d':' -f1)
                new_filename=\"\$(basename \$file)\"
                echo \"Copying \$file to $_remote_tmp_aggregate_dir/\$new_filename\"
                cp \$file $_remote_tmp_aggregate_dir/\$new_filename
            done
        done
        rm -f /tmp/gpufi_interesting_results_$host.tar.gz
        tar czf $_tar_filename --directory $(dirname $_remote_tmp_aggregate_dir) $(basename $_remote_tmp_aggregate_dir)
        rm -rf $_remote_tmp_aggregate_dir
        "
        echo "Done"

        echo -n "Fetching interesting runs from $host..."
        scp "$host":"$_tar_filename" "$LOCAL_BASE_DIR/$host"
        # Cleanup
        ssh "$host" "rm -f \"$_tar_filename\""
        echo "Done"

        # Increment index
        host_index=$((host_index + 1))
    done
}

# Main script
# Parse command line arguments -- use <key>=<value> to override any variable declared above.
for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"
    eval "$KEY=\"$VALUE\""
done

preliminary_checks
remote_copy_filter_script
aggregate_and_fetch_interesting_results
remote_remove_filter_script
