#!/bin/bash
#
# gpuFI helper script to read the results' CSV and only keep the interesting runs.
#
# Assumes that the results.csv provided is inside the .gpuFI/<GPU ID>/<sanitized executable args>/results directory
# starting from the directory where the CUDA executable is.

set -e

# Overridable options
RESULTS_CSV_FILEPATH=
RESULTS_CSV_CLEAN=
# Also delete archived configs
DO_REMOVE_ARCHIVED_CONFIGS=0
DO_CREATE_CLEAN_CSV=0
SHOW_PROGRESS=0

preliminary_checks() {
    if [ -z "$RESULTS_CSV_FILEPATH" ]; then
        echo "Please provide a CSV file to read via the RESULTS_CSV_FILEPATH argument."
        exit 1
    fi
    if [ ! -f "$RESULTS_CSV_FILEPATH" ]; then
        echo "File $RESULTS_CSV_FILEPATH does not exist, please provide a valid CSV file."
        exit 1
    fi
    if [ $DO_CREATE_CLEAN_CSV -eq 1 ] && [ -z "$RESULTS_CSV_CLEAN" ]; then
        RESULTS_CSV_CLEAN="${RESULTS_CSV_FILEPATH}_clean.csv"
        echo "No RESULTS_CSV_CLEAN specified, we will output to $RESULTS_CSV_CLEAN"
    fi
}

# Deletes a single archived config file, given a run_id. Assumes that
# configs are stored under ./configs in the same directory where the results.csv is.
_delete_config_for_run_id() {
    local run_id=$1
    local config_file_path="$(dirname $RESULTS_CSV_FILEPATH)/configs/$run_id.tar.gz"
    if [ -f "$config_file_path" ]; then
        rm -f "$config_file_path"
    fi
}

_get_interesting_runs_from_results() {
    local line_num=0
    local total_lines=$(wc -l "$RESULTS_CSV_FILEPATH" | cut -d' ' -f1)
    local num_cols=0
    local interesting_runs=()
    if [ $SHOW_PROGRESS -eq 1 ]; then
        echo
    fi
    while read -r line; do
        line_num=$((line_num + 1))
        if [ $SHOW_PROGRESS -eq 1 ]; then
            echo -e "\e[1A\e[KReading line $line_num/$total_lines"
        fi
        if [ $line_num -eq 1 ]; then
            # Count columns by using the first line
            num_cols=$(echo "$line" | grep --only-matching "," | wc -l)
            num_cols=$((num_cols + 1))
            # Add header to lines
            # interesting_runs+=($line)
            continue
        fi

        # Split line, parse results
        local run_id
        local success_msg
        local cycles_correct
        local failure_msg
        local syntax_error
        local tag_bitflip
        local l1i_data_bitflip
        local false_l1i_hit_grep
        local different_l1i_misses

        # Split line by commas
        IFS="," read -ra split_line <<<"$line"

        run_id="${split_line[0]}"
        success_msg=$(("${split_line[1]}"))
        cycles_correct=$(("${split_line[2]}"))
        failure_msg=$(("${split_line[3]}"))

        if [ $num_cols -gt 4 ]; then
            syntax_error=$(("${split_line[4]}"))
        fi
        if [ $num_cols -gt 5 ]; then
            tag_bitflip=$(("${split_line[5]}"))
        fi
        if [ $num_cols -gt 6 ]; then
            l1i_data_bitflip=$(("${split_line[6]}"))
        fi
        if [ $num_cols -gt 7 ]; then
            false_l1i_hit_grep=$(("${split_line[7]}"))
        fi
        if [ $num_cols -gt 8 ]; then
            different_l1i_misses=$(("${split_line[8]}"))
        fi

        # Selection of interesting runs
        line=$(echo $line | cut -d',' -f1)
        if [ $success_msg -eq 0 ] || [ $cycles_correct -eq 0 ]; then
            interesting_runs+=("$line")
            continue
        elif [ -n "$syntax_error" ] && [ $syntax_error -eq 1 ]; then
            interesting_runs+=("$line")
            continue
        # Tag bitflips are not that interesting, unless they change the cycles.
        # elif [ -n "$tag_bitflip" ] && [ $tag_bitflip -eq 1 ]; then
        #     interesting_runs+=("$line")
        #     continue
        elif [ -n "$l1i_data_bitflip" ] && [ $l1i_data_bitflip -eq 1 ]; then
            interesting_runs+=("$line")
            continue
        elif [ -n "$false_l1i_hit_grep" ] && [ $false_l1i_hit_grep -eq 1 ]; then
            interesting_runs+=("$line")
            continue
        elif [ -n "$different_l1i_misses" ] && [ $different_l1i_misses -eq 1 ]; then
            interesting_runs+=("$line")
            continue
        elif [ $failure_msg -eq 1 ]; then
            interesting_runs+=("$line")
            continue
        elif [ $DO_REMOVE_ARCHIVED_CONFIGS -eq 1 ]; then
            # Uninteresting run, delete config
            _delete_config_for_run_id $run_id
        fi
    done <"$RESULTS_CSV_FILEPATH"
    if [ $SHOW_PROGRESS -eq 1 ]; then
        echo -en "\e[1A\e[K"
    fi
    export INTERESTING_RUNS="${interesting_runs[*]}"
}

### Main script execution sequence ###
# Parse command line arguments -- use <key>=<value> to override any variable declared above.
for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"
    eval "$KEY=\"$VALUE\""
done

preliminary_checks
_get_interesting_runs_from_results
interesting_runs=(${INTERESTING_RUNS// / })
if [ ${#interesting_runs} -eq 1 ]; then
    echo "No interesting runs found in $RESULTS_CSV_FILEPATH"
    exit 1
fi

# Output clean results to a clean csv
if [ $DO_CREATE_CLEAN_CSV -eq 1 ]; then
    (
        for run in "${interesting_runs[@]}"; do
            echo $run
        done
    ) >"$RESULTS_CSV_CLEAN"
fi
for run in "${interesting_runs[@]}"; do
    echo $run
done
