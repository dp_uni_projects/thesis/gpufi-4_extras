import re
import math
from enum import Enum
from pathlib import Path


class RawFITRates(Enum):
    SM7_QV100 = 1.8 * 10**-6  # 12 nm, https://doi.org/10.1109/ISPASS55109.2022.00004
    SM6_TITANX = 20.49 * 10**-6  # 16 nm, https://doi.org/10.1145/3126908.3126964
    SM7_QV100_8K_8_8 = SM7_QV100
    SM7_QV100_16K_16_8 = SM7_QV100
    SM7_QV100_32K_32_8 = SM7_QV100
    SM7_QV100_64K_64_8 = SM7_QV100
    SM7_QV100_64K_64_8_8SM = SM7_QV100
    SM7_QV100_64K_64_8_16SM = SM7_QV100
    SM7_QV100_64K_64_8_32SM = SM7_QV100


class CacheTypes(Enum):
    """
    An enum holding possible GPU cache types.
    The value of each type is also the gpgpusim.config string
    to search for to get the respective cache configuration.
    """

    L1I = "-gpgpu_cache:il1"
    L1D = "-gpgpu_cache:dl1"
    L1T = "-gpgpu_tex_cache:l1"
    L1C = "-gpgpu_const_cache:l1"
    L2 = "-gpgpu_cache:dl2"


class CacheConfig:
    """
    Class for holding the cache config for one type of GPU cache, parsed
    from a gpgpusim.config

    Call calculate() to get the total size of the cache.
    """

    # GPU Addressing mode. Assume 64 bits always.
    ADDRESS_WIDTH = 64

    def __init__(self, type: CacheTypes):
        self.type: CacheTypes = type
        self.has_sub_partitions: bool = False
        if self.type == CacheTypes.L2:
            self.has_sub_partitions = True
        self.num_sets: int = 0
        self.is_sectored: bool = False
        self.bytes_per_line: int = 0
        self.associativity: int = 0
        self.tag_bits: int = 0
        self.bits_for_sets_indexing: int = 0
        self.bits_for_bytes_offset: int = 0
        # Total bits, per SIMT core.
        # Does not apply for L2.
        self.total_bits_per_core: int = 0
        # Total bits, including tag bits
        # If L1, this is the sum of all cores
        self.total_bits: int = 0
        # Total bytes, excluding tag bits
        self.total_bytes: int = 0
        self.total_bytes_per_core: int = 0
        self.num_sectors: int = 0
        self.num_mem_controllers: int = 0
        self.num_sub_partitions: int = 0
        self.num_simt_cores: int = 0

    @staticmethod
    def __print_bits_in_bytes(bit_value: int) -> str:
        bytes_value = int(bit_value / 8)
        return CacheConfig.__print_bytes(bytes_value)

    @staticmethod
    def __print_bytes(bytes_value: int) -> str:
        # if bit_value > 2**20:
        #     return f"{bit_value >> 20} MB"
        # elif

        if bytes_value > 2**10:
            return f"{bytes_value >> 10} KB"
        return f"{bytes_value} bits"

    def __str__(self) -> str:
        return (
            f"""{self.type.name} Cache:
{20 * '*'}
Number of sets: {self.num_sets}
Bytes per line: {self.bytes_per_line}
Associativity: {self.associativity}
Tag Bits: {self.tag_bits}
Num SIMT cores: {self.num_simt_cores}
Bits for sets indexing: {self.bits_for_sets_indexing}
Bits for byte offset: {self.bits_for_bytes_offset}
"""
            + (
                f"""Number of sectors: {self.num_sectors}
"""
                if self.is_sectored
                else ""
            )
            + (
                f"""Number of mem controllers: {self.num_mem_controllers}
Number of partitions per controller: {self.num_sub_partitions}
Total size (w/o tags): {self.__print_bytes(self.total_bytes)}
Total size (w/tags): {self.__print_bits_in_bytes(self.total_bits)}
"""
                if self.has_sub_partitions
                else f"""Total size per SIMT core (w/o tags): {self.__print_bytes(self.total_bytes_per_core)}
Total size per SIMT core (w/ tags): {self.__print_bits_in_bytes(self.total_bits_per_core)}
Total size (w/o tags): {self.__print_bytes(self.total_bytes)}
Total size (w/ tags): {self.__print_bits_in_bytes(self.total_bits)}
"""
            )
        )

    def calculate(self):
        self.bits_for_sets_indexing = int(math.log2(self.num_sets))
        self.bits_for_bytes_offset = int(math.log2(self.bytes_per_line))
        self.tag_bits = (
            self.ADDRESS_WIDTH
            - self.bits_for_bytes_offset
            - self.bits_for_sets_indexing
        )
        # Size calculation, not taking into account caches which are present in multiple
        # memory controllers (i.e. L2)
        if self.is_sectored:
            self.total_bits = (
                (self.bytes_per_line * 8) * self.associativity * self.num_sets
            )
            # Assume one tag field per sector, i.e. divide total cache lines by sector size.
            self.total_bits = self.total_bits + self.tag_bits * (
                int((self.associativity * self.num_sets) / self.num_sectors)
            )
        else:
            self.total_bits = (
                (self.bytes_per_line * 8 + self.tag_bits)
                * self.associativity
                * self.num_sets
            )
        if self.has_sub_partitions:
            # There's one cache instance per memory subpartition, each one having the
            # configuration given by the gpgpu_cache option in gpgpusim.config.
            # Having calculated the total bits, we can calculate the total cache
            # size by multiplying with the number of mem controllers and the number
            # of sub partitions that each controller has.
            self.total_bits = (
                self.total_bits * self.num_mem_controllers * self.num_sub_partitions
            )
            self.total_bytes = (
                self.num_sets
                * self.associativity
                * self.bytes_per_line
                * self.num_mem_controllers
                * self.num_sub_partitions
            )
        else:
            self.total_bits_per_core = self.total_bits
            # Total bits for each cache type (except L2) are the sum of all
            # cache instances for all SIMT cores.
            self.total_bits *= self.num_simt_cores
            self.total_bytes_per_core = (
                self.num_sets * self.associativity * self.bytes_per_line
            )
            self.total_bytes = self.total_bytes_per_core * self.num_simt_cores


class CacheConfigParser:
    def __init__(self, config_path: Path, hardware_model_path: Path = None):
        if not config_path.is_file():
            raise Exception(f"{config_path} does not exist")
        self._config_path = config_path
        self._hardware_model_path = Path(
            self._config_path.parent.parent.parent.parent,
            "src",
            "abstract_hardware_model.h",
        )
        if hardware_model_path:
            self._hardware_model_path: Path = hardware_model_path
        if not self._hardware_model_path.is_file():
            raise Exception(f"{hardware_model_path} does not exist")
        self._data: dict[str, CacheConfig] = {}
        self.load()

    @property
    def data(self) -> dict[str, CacheConfig]:
        return self._data

    @staticmethod
    def __match_regex_or_raise(rgx: str, string: str) -> re.Match:
        match = re.search(rgx, string, flags=re.M)
        if not match:
            raise Exception(f"Failed matching {rgx} in {string}")
        return match

    def load(self):
        """
        Create a CacheConfig, load the gpgpusim.config and parse the required options,
        updating the CacheConfig. Then, call the calculate() method.
        """
        regex_num_mem_controllers = r"^-gpgpu_n_mem\s*(?P<num_mem_controllers>\d+)"
        regex_num_sub_partitions = (
            r"^-gpgpu_n_sub_partition_per_mchannel\s*(?P<num_sub_partitions>\d+)"
        )
        regex_sector_chunk_size = (
            r"SECTOR_CHUNCK_SIZE\s*=\s*(?P<sector_chunk_size>\d+);"
        )
        regex_num_clusters = r"^-gpgpu_n_clusters\s*(?P<num_clusters>\d+)"
        regex_num_cores_per_cluster = (
            r"^-gpgpu_n_cores_per_cluster\s*(?P<num_cores_per_cluster>\d+)"
        )
        with open(self._config_path, "r") as f:
            config_contents = f.read()
            for cache_type in CacheTypes:
                cache_obj = CacheConfig(cache_type)
                cache_config_regex = f"^{cache_type.value}\\s*(?P<sectored>\\w)?:?(?P<sets>\\d+):(?P<bytes_per_line>\\d+):(?P<associativity>\\d+),"

                parsed_cache_config = self.__match_regex_or_raise(
                    cache_config_regex, config_contents
                )
                parsed_num_clusters = self.__match_regex_or_raise(
                    regex_num_clusters, config_contents
                )
                parsed_num_cores_per_cluster = self.__match_regex_or_raise(
                    regex_num_cores_per_cluster, config_contents
                )
                cache_obj.num_simt_cores = int(
                    parsed_num_clusters.group("num_clusters")
                ) * int(parsed_num_cores_per_cluster.group("num_cores_per_cluster"))

                cache_obj.num_sets = int(parsed_cache_config.group("sets"))
                cache_obj.bytes_per_line = int(
                    parsed_cache_config.group("bytes_per_line")
                )
                cache_obj.associativity = int(
                    parsed_cache_config.group("associativity")
                )
                if parsed_cache_config.group("sectored") == "S":
                    cache_obj.is_sectored = True
                    with open(self._hardware_model_path, "r") as hm:
                        hm_contents = hm.read()
                        sector_match = self.__match_regex_or_raise(
                            regex_sector_chunk_size, hm_contents
                        )

                    cache_obj.num_sectors = int(sector_match.group("sector_chunk_size"))
                if cache_obj.has_sub_partitions:
                    mem_controllers_match = self.__match_regex_or_raise(
                        regex_num_mem_controllers, config_contents
                    )
                    cache_obj.num_mem_controllers = int(
                        mem_controllers_match.group("num_mem_controllers")
                    )

                    sub_partitions_match = self.__match_regex_or_raise(
                        regex_num_sub_partitions, config_contents
                    )
                    cache_obj.num_sub_partitions = int(
                        sub_partitions_match.group("num_sub_partitions")
                    )

                cache_obj.calculate()
                self._data[cache_type.name] = cache_obj


if __name__ == "__main__":

    def print_config(d: dict):
        for k, v in d.items():
            if k == CacheTypes.L1I.name:  # or k == CacheTypes.L1D.name:
                print(v)

    for p in [
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM6_TITANX/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100_8K_8_8/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100_16K_16_8/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100_32K_32_8/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100_64K_64_8/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100_64K_64_8_8SM/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100_64K_64_8_16SM/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100_64K_64_8_32SM/gpgpusim.config",
        "/home/dpapagiannis/Documents/workspace/gpuFI-4/configs/tested-cfgs/SM7_QV100/gpgpusim.config",
    ]:
        print(p)
        print_config(CacheConfigParser(Path(p)).data)
