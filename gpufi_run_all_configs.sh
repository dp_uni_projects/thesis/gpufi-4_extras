#!/bin/bash

# Yet another script for running all benchmark combinations once

set -ex
source ./benchmark_config.sh

prev_dir=$PWD
GPUFI_SOURCE="$HOME/workspace/gpuFI-4"
# Set to 1 if you want to rerun already executed benchmarks anyway
IGNORE_DONE=0

copy_update_config() {
    gpu_config_file=${1:-no config specified}
    cp "$gpu_config_file" "$GPUFI_SOURCE"
    sed -i "s/^-gpufi_profile.*$/-gpufi_profile 3/" "$GPUFI_SOURCE/gpgpusim.config"
    if grep -q "gpufi_l1i_print_stats_per_set" "$GPUFI_SOURCE/gpgpusim.config"; then
        sed -i "s/^-gpufi_l1i_print_stats_per_set.*$/-gpufi_l1i_print_stats_per_set 1/" "$GPUFI_SOURCE/gpgpusim.config"
    else
        printf "\n-gpufi_l1i_print_stats_per_set 1\n" >>"$GPUFI_SOURCE/gpgpusim.config"
    fi
}

# MAIN SCRIPT
for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"
    eval "$KEY=\"$VALUE\""
done

cd "$GPUFI_SOURCE"
source "./startup.sh" release

for gpu_config in "${gpu_configs[@]}"; do
    gpu_config_file="$GPUFI_SOURCE/configs/tested-cfgs/$gpu_config/gpgpusim.config"
    for exe_args in "${name_exe_and_args[@]}"; do
        name=$(echo $exe_args | cut -d',' -f1)
        exe=$(echo $exe_args | cut -d',' -f2)
        args=$(echo $exe_args | cut -d',' -f3)
        # echo $gpu_config $exe $args
        if [ -f ".${name}_${gpu_config}_done" ] && [ $IGNORE_DONE -eq 0 ]; then
            echo "Benchmark ${name} for ${gpu_config} already executed"
            continue
        fi
        copy_update_config "$gpu_config_file"
        eval $exe "$args" >"${name}_${gpu_config}.log"
        touch ".${name}_${gpu_config}_done"
    done
done

cd $prev_dir
