BENCHMARK_ALIASES = {
    "kmeans": "KM",
    "srad_v1": "SRAD_v1",
    "srad_v2": "SRAD_v2",
    "hotspot_256": "HS_256",
    "hotspot_512": "HS_512",
    "lud": "LUD",
    "bfs": "BFS",
    "pathfinder": "PATHF",
    "nw": "NW",
    "gaussian": "GE",
    "backprop": "BP",
}

GPU_ALIASES = {
    "SM6_TITANX": "Titan X",
    "SM7_QV100": "Quadro V100",
    "SM7_QV100_8K_8_8": "Quadro V100 (640K)",
    "SM7_QV100_16K_16_8": "Quadro V100 (1280K)",
    "SM7_QV100_32K_32_8": "Quadro V100 (2560K)",
    "SM7_QV100_64K_64_8_8SM": "Quadro V100 (512K)",
    "SM7_QV100_64K_64_8_16SM": "Quadro V100 (1024K)",
    "SM7_QV100_64K_64_8_32SM": "Quadro V100 (2048K)",
    "SM7_QV100_64K_64_8": "Quadro V100 (5120K)",
}
