#!/bin/bash
#
# gpuFI helper script for extracting per-L1I cache usage from a GPGPU-Sim log file.
# This information is being printed from gpuFI, in order to profile the rodinia benchmarks.
#
set -e

GPUFI_LOG_FILE=
AVG_USAGE_OUTPUT_FILE=
ACCESSES_PER_SET_OUTPUT_FILE=

preliminary_checks() {
    if [ -z "$GPUFI_LOG_FILE" ]; then
        echo "No log file was supplied with GPUFI_LOG_FILE"
        exit 1
    fi

    if [ -n "$GPUFI_LOG_FILE" ] && [ ! -f "$GPUFI_LOG_FILE" ]; then
        echo "File $GPUFI_LOG_FILE not found!"
        exit 1
    fi

    AVG_USAGE_OUTPUT_FILE=avg_l1i_usage_$(basename $GPUFI_LOG_FILE | cut -d'.' -f1).csv
    ACCESSES_PER_SET_OUTPUT_FILE=accesses_per_set_$(basename $GPUFI_LOG_FILE | cut -d'.' -f1).csv
}

extract_stats() {
    kernel_names=$(grep -E "kernel_name = " "$GPUFI_LOG_FILE" | cut -d"=" -f2 | cut -d" " -f2)
    kernel_names=(${kernel_names// / })
    kernel_cycles=$(grep -E "gpu_tot_sim_cycle = " "$GPUFI_LOG_FILE" | cut -d"=" -f2 | cut -d" " -f2)
    kernel_cycles=(${kernel_cycles// / })
    average_l1i_usage=$(grep -E "L1I_average_usage = [[:digit:]]{1,3}\.[[:digit:]]{2}%" "$GPUFI_LOG_FILE" | cut -d"=" -f2 | cut -d" " -f2 | cut -d"%" -f1)
    average_l1i_usage=(${average_l1i_usage// / })

    # Get num of SMs and sets to manually index set accesses output later. Too hacky.
    num_sms=$(($(grep -oE 'L1I_cache_core\[[0-9]+\]' "$GPUFI_LOG_FILE" | sort -u | wc -l)))
    num_sets=$(($(grep -oE 'Set\[[0-9]+\]' "$GPUFI_LOG_FILE" | sort -u | wc -l)))
    all_sets_accesses=$(gawk -v pat='L1I_cache_core\\[([0-9]+)\\],Set\\[([0-9]+)\\]: Access = ([0-9]+)' 'match($0, pat, a) {print a[1] "," a[2] "," a[3]}' <"$GPUFI_LOG_FILE")
    # Split gawk output by lines, each line will be like: <sm>,<set>,<accesses>
    readarray -t all_sets_accesses <<<"$all_sets_accesses"

    # Create avg usage file
    echo "cycle,kernel_name,average_l1i_usage" >"$AVG_USAGE_OUTPUT_FILE"
    # Create sets accesses file
    echo "cycle,kernel_name,sm,set,accesses" >"$ACCESSES_PER_SET_OUTPUT_FILE"

    # Loop over all kernel invocations found
    for i in $(seq 0 $((${#kernel_names[@]} - 1))); do
        # Write to avg usage file
        echo "${kernel_cycles[$i]},${kernel_names[$i]},${average_l1i_usage[$i]}" >>"$AVG_USAGE_OUTPUT_FILE"

        # Now get the accesses by calculating the index of the line of gawk output.
        start=$((i * (num_sms * num_sets)))
        finish=$(((i + 1) * (num_sms * num_sets)))
        # printf '%s\n' "${all_sets_accesses[@]:$start:$finish}" >>"$ACCESSES_PER_SET_OUTPUT_FILE"
        # Write to sets accesses file
        for el in "${all_sets_accesses[@]:$start:$finish}"; do
            echo "${kernel_cycles[$i]},${kernel_names[$i]},$el" >>"$ACCESSES_PER_SET_OUTPUT_FILE"
        done
        # printf '%s,%s\n' "${kernel_cycles[$i]},${kernel_names[$i]}" "${all_sets_accesses[@]:$start:$finish}" >>"$ACCESSES_PER_SET_OUTPUT_FILE"
    done
    #

}

### Main script execution sequence ###
# Parse command line arguments -- use <key>=<value> to override any variable declared above.
for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"
    eval "$KEY=\"$VALUE\""
done

preliminary_checks
extract_stats
