from functools import cache, cached_property
import os
import re
from pathlib import Path
import pandas
import math
from copy import copy
import plotly.graph_objects as go
from utils import BENCHMARK_ALIASES, GPU_ALIASES
from latex_tools import LatexTools
from cache_tools import CacheConfigParser, CacheConfig, CacheTypes, RawFITRates


class gpuFIResult:
    """
    Class managing a single GPU/Benchmark/Args results.csv file
    """

    P_PARAMETER = 0.5  # p, found in https://doi.org/10.1109/DATE.2009.5090716
    T_PARAMETER = 2.5758  # t, confidence level 99%, source as above

    def __init__(
        self,
        gpu_config_name: str,
        benchmark_name: str,
        benchmark_args: str,
        results_filepath: Path,
        gpu_cache_config: CacheConfig,
    ):
        self._results_filepath = results_filepath
        self._gpu_config_name = gpu_config_name
        self._benchmark_name = benchmark_name
        self._benchmark_args = benchmark_args
        self._gpu_cache_config = gpu_cache_config
        self._data = None
        self._is_loaded = False

    @staticmethod
    def is_valid_results_file(filepath: Path):
        with open(filepath, "r") as f:
            return (
                f.readline().strip()
                == "run_id,success,same_cycles,failed,syntax_error,tag_bitflip,l1i_data_bitflip,false_l1i_hit,different_l1i_misses"
            )

    @staticmethod
    def must_be_loaded(func):
        """
        Decorator for protecting methods from running
        if no data has been loaded.
        """

        def wrapper(*args, **kwargs):
            if not args[0]._is_loaded:
                raise Exception("No data loaded. Please run load() first.")
            else:
                return func(*args, **kwargs)

        return wrapper

    def __str__(self):
        return f"{self._benchmark_name} @ {self._gpu_config_name}, {self.num_samples} samples"

    @cached_property
    def num_samples(self):
        return self._data.shape[0] if self._data is not None else 0

    def load(self):
        try:
            # Reset cached num_samples number
            del self.num_samples
        except:
            pass
        self.calculate.cache_clear()
        self._data = pandas.read_csv(self._results_filepath)
        self._is_loaded = True

    @cache
    @must_be_loaded
    def calculate(self, max_samples: int, use_raw_fit: bool = False) -> dict:
        num_samples = self.num_samples
        # Keep what is lesser, max_samples or the length of the dataframe.
        if max_samples > 0 and max_samples < self.num_samples:
            num_samples = max_samples
        data = self._data[:num_samples]
        # Do the actual calculations for this result
        num_failures = len(data[data["success"] != 1])
        avf = num_failures / num_samples
        rawfit = (
            RawFITRates.__members__[self._gpu_config_name].value
            if (use_raw_fit and self._gpu_config_name in RawFITRates.__members__)
            else 1
        )
        fit = avf * self._gpu_cache_config.total_bits * rawfit
        num_dues = len(data[(data["success"] != 1) & (data["failed"] != 1)])
        num_dues_percent = num_dues / num_samples
        num_sdcs = len(data[(data["success"] != 1) & (data["failed"] == 1)])
        num_sdcs_percent = num_sdcs / num_samples
        num_masked = len(data[(data["success"] == 1)])
        num_masked_percent = num_masked / num_samples
        # https://doi.org/10.1109/DATE.2009.5090716
        error = math.sqrt(
            self.T_PARAMETER
            * ((self.P_PARAMETER * (1 - self.P_PARAMETER)) / num_samples)
            * (
                (self._gpu_cache_config.total_bits - num_samples)
                / (self._gpu_cache_config.total_bits - 1)
            )
        )
        # In some cases, even though no bitflip takes place, the simulator
        # reports success and different number of cycles, which doesn't make
        # sense, since GPGPU-Sim is deterministic.
        # Re-running those weird runs does not reproduce the result,
        # so it might have to do with something inherent in the simulator.
        num_unidentified_errors = len(
            data[
                (data["success"] == 1)
                & (data["same_cycles"] == 0)
                & (data["l1i_data_bitflip"] == 0)
                & (data["tag_bitflip"] == 0)
            ]
        )
        # Print a warning if such errors are detected.
        if num_unidentified_errors > 0:
            print(
                f"{self._benchmark_name}@{self._gpu_config_name} has {num_unidentified_errors} unidentified errors, probably needs to be re-run"
            )
        assert num_dues + num_sdcs + num_masked == num_samples
        return dict(
            num_samples=num_samples,
            avf=avf,
            fit=fit,
            due=dict(total=num_dues, percent=num_dues_percent),
            sdc=dict(total=num_sdcs, percent=num_sdcs_percent),
            masked=dict(total=num_masked, percent=num_masked_percent),
            error=error,
        )


class gpuFIResultsManager:
    """
    Class to manage all gpuFIResult instances.
    Given a base path, automatically detects and loads result files.
    """

    # Aliases used to replace the benchmark names
    # in the generated plots, to match the thesis names
    BENCHMARK_ALIASES = BENCHMARK_ALIASES

    GPU_ALIASES = GPU_ALIASES

    def __init__(self, base_path: Path, gpu_configs_base_path: Path):
        # The _results_by_gpu attribute is where we store the gpuFIResult instances
        # indexed by gpu and then benchmark
        self._results_by_gpu: dict[str, dict[str, gpuFIResult]] = {}
        # A similar results dict, but indexed by benchmark, then gpu. There's
        # probably a smarter way to do that as well. The gpuFIResult instances
        # are not ducpilcated, since the results dicts hold references only.
        self._results_by_benchmark: dict[str, dict[str, gpuFIResult]] = {}
        if not os.path.isdir(base_path):
            raise Exception(f"{base_path} does not exist")
        # Base directory where all the results are to be found
        self._base_path: Path = base_path
        # Base directory where all the gpu configs are to be found
        self._gpu_configs_base_path: Path = gpu_configs_base_path
        # List of all the benchmark names loaded
        self._benchmark_names: list[str] = []
        # List of all the GPU config names loaded
        self._gpu_config_names: list[str] = []
        # Cache configurations for all available GPUs
        # dict[GPU, [cache type, cache config]]
        self._gpu_cache_configs: dict[str, dict[str, CacheConfig]] = {}

        # Regex for gpu config folders
        self.gpu_config_directory_regex = re.compile("^SM[0-9]{1,4}_.+")

        # Initialize
        self.__load_gpu_configs()
        self.__search_for_results()

    def __is_valid_gpu_dir(self, dirname: str):
        match = re.match(self.gpu_config_directory_regex, dirname)
        return match is not None

    def contents(self, sort="benchmark"):
        """
        Prints the number of samples for each result stored.
        Can be sorted by benchmark or by gpu config.
        """
        print(20 * "=")
        if sort == "benchmark":
            for benchmark, gpus in self._results_by_benchmark.items():
                print(benchmark)
                for gpu, results in gpus.items():
                    print(f"\t{gpu}: {results.num_samples}")
                print()
        elif sort == "gpu":
            for gpu, benchmarks in self._results_by_gpu.items():
                print(gpu)
                for benchmark, results in benchmarks.items():
                    print(f"\t{benchmark}: {results.num_samples}")
                print()

    def __search_for_results(self):
        """
        Given the configured base path, searches for valid csv result files
        and stores them judging on the file path, assuming that the folder structure follows
        the convention benchmark_name/gpu_config/args
        """
        for root, _, files in os.walk(self._base_path):
            if not files:
                continue
            for file in files:
                if file.lower().endswith(".csv") and gpuFIResult.is_valid_results_file(
                    Path(root, file)
                ):
                    args = os.path.basename(root)
                    gpu_config_name = os.path.basename(os.path.dirname(root))
                    benchmark_name = os.path.basename(
                        os.path.dirname(os.path.dirname(root))
                    )
                    # Add the names to lists for easier future use
                    if gpu_config_name not in self._gpu_config_names:
                        self._gpu_config_names.append(gpu_config_name)
                    if benchmark_name not in self._benchmark_names:
                        self._benchmark_names.append(benchmark_name)

                    # Populate result dicts
                    if benchmark_name not in self._results_by_benchmark:
                        self._results_by_benchmark[benchmark_name] = {}
                    if (
                        gpu_config_name
                        not in self._results_by_benchmark[benchmark_name]
                    ):
                        self._results_by_benchmark[benchmark_name][gpu_config_name] = {}
                    if gpu_config_name not in self._results_by_gpu:
                        self._results_by_gpu[gpu_config_name] = {}
                    if benchmark_name not in self._results_by_gpu[gpu_config_name]:
                        self._results_by_gpu[gpu_config_name][benchmark_name] = {}
                    self._results_by_gpu[gpu_config_name][benchmark_name] = gpuFIResult(
                        benchmark_name=benchmark_name,
                        gpu_config_name=gpu_config_name,
                        benchmark_args=args,
                        results_filepath=Path(root, file),
                        gpu_cache_config=self._gpu_cache_configs[gpu_config_name][
                            CacheTypes.L1I.name
                        ],
                    )
                    self._results_by_benchmark[benchmark_name][gpu_config_name] = (
                        self._results_by_gpu[gpu_config_name][benchmark_name]
                    )

    def __load_gpu_configs(self):
        """
        Given the configured base path, searches for valid gpgpusim.configs
        """
        for root, _, files in os.walk(self._gpu_configs_base_path):
            if not files:
                continue
            if "gpgpusim.config" not in files:
                continue
            gpu_name = os.path.basename(root)
            if not self.__is_valid_gpu_dir(gpu_name):
                continue
            self._gpu_cache_configs[gpu_name] = CacheConfigParser(
                Path(root, "gpgpusim.config")
            ).data

    def load(self):
        """
        For each gpuFIResult, load the results to pandas
        and calculate the values we want.
        """
        for gpu_config_name, benchmarks in self._results_by_gpu.items():
            for benchmark_name, _ in benchmarks.items():
                self._results_by_gpu[gpu_config_name][benchmark_name].load()

    def __create_plot(
        self,
        x_data: list[str],
        y_data: list[list[float]],
        bar_text: list[list[str]] = [[""]],
        series_names: list[str] = [""],
        title: str = "",
        xlabel: str = "",
        ylabel: str = "",
        **kwargs,
    ) -> go.Figure:
        """
        Generic method to show bar plots in a unified way.
        The figure, bar plot and traces configurations can be overriden through kwargs.
        Suffers from https://en.m.wikipedia.org/wiki/Inner-platform_effect
        """
        # Allow overriding figure and bar plot args from outside
        xaxis_kwargs = dict(dtick=1, title=xlabel)
        if "xaxis_kwargs" in kwargs:
            xaxis_kwargs.update(**kwargs["xaxis_kwargs"])
        yaxis_kwargs = dict(
            dtick=1,
            title=ylabel,
            tickmode="auto",
            showexponent="all",
            exponentformat="E",
        )
        if "yaxis_kwargs" in kwargs:
            yaxis_kwargs.update(**kwargs["yaxis_kwargs"])

        fig_kwargs = dict(
            title={"text": title},
            xaxis=xaxis_kwargs,
            yaxis=yaxis_kwargs,
            width=500,
            height=500,
        )
        if "fig_kwargs" in kwargs:
            fig_kwargs.update(**kwargs["fig_kwargs"])

        bar_kwargs = dict(
            x=x_data,
            textposition="auto",
            # text=bar_text,
        )
        if "bar_kwargs" in kwargs:
            bar_kwargs.update(**kwargs["bar_kwargs"])

        traces_kwargs = dict(
            # textposition="outside",
            # textfont_size=12,
            # textangle=0,
        )
        if "traces_kwargs" in kwargs:
            traces_kwargs.update(**kwargs["traces_kwargs"])
        fig = go.Figure(
            [
                go.Bar(**bar_kwargs, y=y, name=series_names[i])
                for i, y in enumerate(y_data)
            ]
        )
        fig.update_layout(**fig_kwargs)
        fig.update_traces(**traces_kwargs)

        return fig

    def __get_latex_tabular(
        self,
        x_data: list,
        y_data: list,
        series_names: list,
        alignment: str = "",
        resize: bool = False,
        value_col_name: str = "Value",
    ) -> str:
        double_backslash = "\\"
        cols = ["GPU config.", "Benchmark", value_col_name]
        if not alignment:
            alignment = "l" * len(cols)
        latex = LatexTools.get_latex_tabular_start(alignment=alignment, resize=resize)
        for col in cols:
            latex += f"{double_backslash}textbf{{{col}}}&"
        latex = latex[:-1]  # Remove final &
        latex += "\\\\"  # EOL
        latex += f"""
        {double_backslash}hline
"""
        # series names = 1D list of gpu configs
        # x_data = 1D list of benchmark names
        # y_data = 2D list of values for gpu config & benchmark name combination
        for name_idx, name in enumerate(series_names):
            has_printed_name = False
            for x_idx, x_datum in enumerate(x_data):
                # Machete to print small numbers with floats, large with ints
                y_datum = y_data[name_idx][x_idx]
                y_datum = (
                    f"{int(y_datum)}"
                    if y_datum > 10
                    else f"{y_data[name_idx][x_idx]:.2f}"
                )
                latex += f"""{LatexTools.escape_underscore(name) if not has_printed_name else '~'}&{LatexTools.escape_underscore(x_datum)}&{y_datum}{double_backslash}{double_backslash}
"""
                has_printed_name = True
            latex += f"""
        {double_backslash}hline
"""
        latex += LatexTools.get_latex_tabular_end(resize=resize)
        return latex

    def print_error_table(
        self,
        max_samples: int,
        gpu_configurations: list = [],
    ):
        """
        Print the error of each campaign, based on GPU L1I cache size
        Based on https://doi.org/10.1109/DATE.2009.5090716
        Left unfinished.
        """
        if not gpu_configurations:
            gpu_configurations = self._gpu_config_names

        # Random benchmark, doesn't make a difference
        for gpu, result in self._results_by_benchmark["hotspot_512"].items():
            if gpu in gpu_configurations:
                print(
                    f"{self.GPU_ALIASES[gpu]} && {result.calculate(max_samples=max_samples)['error'] * 100:.2f}"
                )

    def print_fit_table(
        self,
        benchmark_names: list[str] = [""],
        gpu_configurations: list = [],
        caption: str = "",
        label: str = "",
        gpu_aliases: dict[str, str] = {},
        max_num_samples: int = math.inf,
        resize: bool = False,
        value_col_name: str = "",
        use_raw_fit: bool = False,
    ):
        """
        Prints a latex table with the FIT of the benchmarks and gpus specified
        """

        # Empty list means all benchmarks
        if not benchmark_names:
            benchmark_names = self._benchmark_names

        if (
            isinstance(benchmark_names, str)
            and benchmark_names in self._benchmark_names
        ):
            benchmark_names = [benchmark_names]

        if isinstance(benchmark_names, list) and not all(
            benchmark in self._benchmark_names for benchmark in benchmark_names
        ):
            raise Exception(f"No {benchmark_names} benchmark found in results")

        # Overwrite GPU aliases if needed
        gpu_aliases_copy = copy(self.GPU_ALIASES)
        gpu_aliases_copy.update(gpu_aliases)
        del gpu_aliases
        if not gpu_configurations:
            gpu_configurations = self._gpu_config_names

        # First figure out the minimum length of all the results available.
        max_num_samples = min(
            *[
                self._results_by_gpu[gpu][benchmark_name].num_samples
                for gpu in gpu_configurations
                for benchmark_name in benchmark_names
            ],
            max_num_samples,
        )
        x_data = [
            self.BENCHMARK_ALIASES[benchmark_name] for benchmark_name in benchmark_names
        ]
        y_data = [
            [
                self._results_by_benchmark[benchmark_name][gpu].calculate(
                    max_samples=max_num_samples, use_raw_fit=use_raw_fit
                )["fit"]
                for benchmark_name in benchmark_names
            ]
            for gpu in gpu_configurations
        ]
        latex = LatexTools.get_latex_table_start(caption=caption, label=label)
        latex += self.__get_latex_tabular(
            x_data,
            y_data,
            series_names=[gpu_aliases_copy[gpu] for gpu in gpu_configurations],
            resize=resize,
            value_col_name=value_col_name,
        )
        latex += LatexTools.get_latex_table_end()
        return latex

    def plot_fit(
        self,
        benchmark_names: list[str] = [""],
        gpu_configurations: list = [],
        caption: str = "",
        label_postfix: str = "",
        save_to_file: bool = False,
        filename_postfix: str = "",
        gpu_aliases: dict[str, str] = {},
        max_num_samples: int = math.inf,
        use_raw_fit: bool = False,
        **kwargs,
    ):
        """
        Calculates and shows a bar plot for FITs for all specified GPU configs for all specified benchmarks.
        gpu_sort allows getting the gpu configs in a user-specified order.
        Returns a latex string for a figure, pointing to the file created, when save_to_file is True.
        """

        # Empty list means all benchmarks
        if not benchmark_names:
            benchmark_names = self._benchmark_names

        if (
            isinstance(benchmark_names, str)
            and benchmark_names in self._benchmark_names
        ):
            benchmark_names = [benchmark_names]

        if isinstance(benchmark_names, list) and not all(
            benchmark in self._benchmark_names for benchmark in benchmark_names
        ):
            raise Exception(f"No {benchmark_names} benchmark found in results")

        # Overwrite GPU aliases if needed
        gpu_aliases_copy = copy(self.GPU_ALIASES)
        gpu_aliases_copy.update(gpu_aliases)
        del gpu_aliases
        if not gpu_configurations:
            gpu_configurations = self._gpu_config_names

        # First figure out the minimum length of all the results available.
        max_num_samples = min(
            *[
                self._results_by_gpu[gpu][benchmark_name].num_samples
                for gpu in gpu_configurations
                for benchmark_name in benchmark_names
            ],
            max_num_samples,
        )
        x_data = [
            self.BENCHMARK_ALIASES[benchmark_name] for benchmark_name in benchmark_names
        ]
        y_data = [
            [
                self._results_by_benchmark[benchmark_name][gpu].calculate(
                    max_samples=max_num_samples, use_raw_fit=use_raw_fit
                )["fit"]
                for benchmark_name in benchmark_names
            ]
            for gpu in gpu_configurations
        ]

        fig = self.__create_plot(
            x_data=x_data,
            y_data=y_data,
            # bar_text=[[f"{result['avf']* 100:.2f}" for result in results]],
            bar_text=[""],
            # title=f"L1I AVF (%) for the {self.BENCHMARK_ALIASES[benchmark_name]} benchmark, {max_num_samples} runs.",
            title=f"L1I FIT rates for the {benchmark_names} benchmarks, {max_num_samples} runs.",
            xlabel="GPU configuration",
            ylabel="FIT rate",
            series_names=[gpu_aliases_copy[gpu] for gpu in gpu_configurations],
            **kwargs,
        )
        fig.show()
        # Slice strings arbitrarily to make sure the filenames are not overly big
        filename = f"./fit_{'_'.join([benchmark_name[-5:] for benchmark_name in benchmark_names])}_{'_'.join(gpu_configuration[-5:] for gpu_configuration in gpu_configurations)}{'_' + filename_postfix if filename_postfix else ''}.pdf"
        if save_to_file:
            # Remove title, not needed for latex
            fig.update_layout(title={"text": None})
            fig.write_image(filename)

        # Get latex str
        label = (
            f"fig:fit-results-benchmarks{'-'+label_postfix if label_postfix else ''}"
        )
        latex_str = LatexTools.get_latex_figure_start(caption=caption, label=label)
        latex_str += LatexTools.get_latex_figure(filename=filename)
        latex_str += LatexTools.get_latex_figure_end()

        return latex_str


if __name__ == "__main__":
    manager = gpuFIResultsManager(
        base_path=Path.home()
        / "Sync"
        / "Documents"
        / "CTN_ENG"
        / "Thesis"
        / "gpuFI-4"
        / "results",
        gpu_configs_base_path=Path.home()
        / "workspace"
        / "gpuFI-4"
        / "configs"
        / "tested-cfgs",
    )
    manager.load()
    manager.contents(sort="gpu")
    # dict[GPU, [Cache type, Cache config]]
    for gpu, cc in manager._gpu_cache_configs.items():
        print(gpu)
        for cache_type, cache_config in cc.items():
            print(cache_type, cache_config.total_bits)
