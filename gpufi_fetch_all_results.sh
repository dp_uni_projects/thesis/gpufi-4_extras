#!/bin/bash

# Script that gathers all results.csv from remote hosts, given the config in benchmark_config.sh
# and aggregates them.
# set -ex

source ./benchmark_config.sh

REMOTE_HOSTS="homePrixtis home5593"

# Single quote so that it doesn't expand variables locally, we expect to find "gpufi_utils.sh" in there.
GPUFI_BASE_PATH='$HOME/workspace/gpuFI-4'
LOCAL_BASE_DIR="$HOME/Sync/Documents/CTN_ENG/Thesis/gpuFI-4/results"

# Function to sanitize args to a folder name
# From here: https://stackoverflow.com/a/44811468/6562491
# echoes "null" if no input given.
_sanitize_string() {
    local s="${*:-null}"     # receive input in first argument
    s="${s//[^[:alnum:]]/-}" # replace all non-alnum characters to -
    s="${s//+(-)/-}"         # convert multiple - to single -
    s="${s/#-/}"             # remove - from start
    s="${s/%-/}"             # remove - from end
    echo "${s,,}"            # convert to lowercase
}

preliminary_checks() {
    REMOTE_HOSTS=($REMOTE_HOSTS)
    if [ "${#REMOTE_HOSTS[@]}" -eq 0 ]; then
        echo "Please specify the REMOTE_HOSTS variable"
        exit 1
    fi
}
fetch_results() {
    for host in "${REMOTE_HOSTS[@]}"; do
        for exe_args in "${name_exe_and_args[@]}"; do
            for gpu_config in "${gpu_configs[@]}"; do
                local name=$(echo $exe_args | cut -d',' -f1)
                local exe=$(echo $exe_args | cut -d',' -f2)
                local args=$(echo $exe_args | cut -d',' -f3)

                local remote_benchmark_dir=$(dirname $exe)
                # To get the proper sanitized path from the args, we need to run the function
                # on the remote host, so that HOME can be expanded properly.
                # MACHETE
                echo -n "Getting remote results path for benchmark $name from $host..."
                local remote_cmd=$(echo "cd "${GPUFI_BASE_PATH}" && source gpufi_utils.sh && _sanitize_string \""$args'"')
                local sanitized_args=$(ssh "$host" $remote_cmd)

                # The final path where results.csv is in the remote host.
                # We need to evaluate the $HOME var remotely first.
                local remote_results_filepath=$(ssh $host 'echo '${remote_benchmark_dir}"/.gpufi/$gpu_config/$sanitized_args/results/results.csv")
                echo "Done."

                # Don't use sanitized args here, so that we can use the same dir for all hosts
                local results_dir="$LOCAL_BASE_DIR/$name/$gpu_config/$(_sanitize_string $args)"
                mkdir -p "$results_dir"
                echo -n "Copying remote file $host:${remote_results_filepath}..."
                scp "$host":"$remote_results_filepath" "$results_dir/results_$host.csv" || echo "ERROR: Could not find remote file $remote_results_filepath"
                echo "Done."
            done
        done
    done
}

# In each results folder, results from all hosts will be found. Merge all csv files into a single results.csv.
merge_results() {
    for exe_args in "${name_exe_and_args[@]}"; do
        for gpu_config in "${gpu_configs[@]}"; do
            local name=$(echo $exe_args | cut -d',' -f1)
            local exe=$(echo $exe_args | cut -d',' -f2)
            local args=$(echo $exe_args | cut -d',' -f3)
            local results_dir="$LOCAL_BASE_DIR/$name/$gpu_config/$(_sanitize_string $args)"
            rm -rf $results_dir/results.csv # Probably leftover from previous script execution

            # Skip empty folders
            if [ $(ls -1 $results_dir | wc -l) -eq 0 ]; then
                echo "No files found in $results_dir, skipping"
                continue
            fi
            # Dump the header from the first file into a results.csv file
            head -1 $results_dir/$(ls -1 $results_dir | tail -1) >$results_dir/results.csv

            # Get all lines after the first one and dump to results.cv
            for file in $results_dir/*; do
                if [ "$(basename $file)" = "results.csv" ]; then
                    continue
                fi
                echo -n "Merging $file into results.csv..."
                # https://stackoverflow.com/questions/34504311/how-to-tail-all-lines-except-first-row
                tail -n+2 $file >>$results_dir/results.csv
                rm $file
                echo "Done."
            done
        done
    done
}

### Main script

declare -a steps=(
    preliminary_checks
    fetch_results
    merge_results
)
for step in "${steps[@]}"; do
    eval "do_${step}=1"
done
# Parse command line arguments -- use <key>=<value> to override any variable declared at the top of the script.
for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"
    eval "$KEY=\"$VALUE\""
done
# The actual procedure.
# For each step, check if the appropriate flag is enabled.
for step in "${steps[@]}"; do
    step_flag_name=do_$step
    if [ "${!step_flag_name}" -ne 0 ]; then
        echo "Step: $step"
        # Run the actual function
        eval "$step"
    else
        echo "Skipping step: $step"
    fi
done
