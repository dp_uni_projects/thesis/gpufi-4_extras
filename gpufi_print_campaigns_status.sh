#!/bin/bash

# Yet another script for full campaigns for all configs and args
# Cannot use -e because We want to check a nonzero return code :/
#set -x
source ./benchmark_config.sh

prev_dir=$PWD
GPUFI_SOURCE="$HOME/workspace/gpuFI-4"


analyze_executables() {
    cd $GPUFI_SOURCE
    source "${GPUFI_SOURCE}/gpufi_utils.sh"
    # Run analysis in parallel?
    for gpu_config in "${gpu_configs[@]}"; do
        # gpu_config_file="$GPUFI_SOURCE/configs/tested-cfgs/$gpu_config/gpgpusim.config"
        for exe_args in "${name_exe_and_args[@]}"; do
            BENCHMARK_NAME=$(echo $exe_args | cut -d',' -f1)
	    GPU_ID=$gpu_config
            CUDA_EXECUTABLE_PATH=$(echo $exe_args | cut -d',' -f2)
            CUDA_EXECUTABLE_ARGS=$(eval echo $exe_args | cut -d',' -f3)
	    results_filename=$(eval "echo $(_get_gpufi_analysis_path)/results/results.csv")
	    if [ -f $results_filename ]; then
		num_results=$(($(wc -l $results_filename | cut -d' ' -f1) - 1))
		echo -e "$GPU_ID $BENCHMARK_NAME $num_results"
	    fi
        done
    done 
    cd - &>/dev/null
}


### Main script
declare -a steps=(
    analyze_executables
)

# Create dynamic flags to selectively disable/enable steps of the analysis if needed.
# Those flags are named "do_" with the name of the function.
# We set those flags to 1 by default.
for step in "${steps[@]}"; do
    eval "do_${step}=1"
done

# Override variables from script
for ARGUMENT in "$@"; do
    KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
    KEY_LENGTH=${#KEY}
    VALUE="${ARGUMENT:$KEY_LENGTH+1}"
    eval "$KEY=\"$VALUE\""
done

#cd "$GPUFI_SOURCE"
#source "./startup.sh" release

# The actual procedure.
# For each step, check if the appropriate flag is enabled.
for step in "${steps[@]}"; do
    step_flag_name=do_$step
    if [ "${!step_flag_name}" -ne 0 ]; then
        echo "Step: $step"
        # Run the actual function
        eval "$step"
    else
        echo "Skipping step: $step"
    fi
done

#cd $prev_dir
