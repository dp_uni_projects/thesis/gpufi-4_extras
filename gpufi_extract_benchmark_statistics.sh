#!/bin/bash
# Script to extract statistics from execution logs found
# in LOCAL_BASE_DIR. This dir should contain per GPU and per
# args subdirs.
# Call this after gpufi_fetch_out_logs.sh
# Creates a csv on stdout.
set -e

source ./benchmark_config.sh
LOCAL_BASE_DIR="$HOME/Sync/Documents/CTN_ENG/Thesis/gpuFI-4/logs"
ATTRS_TO_EXTRACT=("gpu_tot_sim_cycle" "L1I_total_cache_accesses" "L1I_total_cache_misses")
# Function to sanitize args to a folder name
# From here: https://stackoverflow.com/a/44811468/6562491
# echoes "null" if no input given.
_sanitize_string() {
    local s="${*:-null}"     # receive input in first argument
    s="${s//[^[:alnum:]]/-}" # replace all non-alnum characters to -
    s="${s//+(-)/-}"         # convert multiple - to single -
    s="${s/#-/}"             # remove - from start
    s="${s/%-/}"             # remove - from end
    echo "${s,,}"            # convert to lowercase
}
# Header
echo "GPU config,benchmark,$(echo ${ATTRS_TO_EXTRACT[@]} | tr ' ' ','), % Misses,"
# Loop over all gpu configs and benchmarks
for gpu_config in "${gpu_configs[@]}"; do
    for exe_args in "${name_exe_and_args[@]}"; do
        name=$(echo $exe_args | cut -d',' -f1)
        args=$(echo $exe_args | cut -d',' -f3)

        log_file="$LOCAL_BASE_DIR/$name/$gpu_config/$(_sanitize_string $args)/out.log"
        # stats_file="$LOCAL_BASE_DIR/$name/$gpu_config/$(_sanitize_string $args)/stats.txt"
        # : >$stats_file # Create an empty stats file. Machete that I saw somewhere (instead of touch?) and wanted to use for no reason
        line=$(echo "$gpu_config,$name,")
        # Machete to calculate percentage
        l1i_total_cache_accesses=0
        for attr in "${ATTRS_TO_EXTRACT[@]}"; do
            # Fill stats file
            # grep "$attr" "$log_file" | tail -1 | xargs >>"$stats_file"
            # Extract value from logs. Keep only the last occurrence.
            value=$(grep $attr $log_file | tail -1 | cut -d'=' -f2 | xargs)
            line="${line}${value},"
            # Machete keeps coming
            if [ "$attr" = "L1I_total_cache_accesses" ]; then
                l1i_total_cache_accesses=$value
            elif [ "$attr" = "L1I_total_cache_misses" ] && [ "$l1i_total_cache_accesses" -ne 0 ]; then
                line="${line}$(printf '%0.2f' $(bc -l <<<"($value / $l1i_total_cache_accesses)*100")),"
            fi
        done
        echo $line
    done
done
